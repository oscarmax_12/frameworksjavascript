var nombre="Oscar Diaz";
var altura=16;

var datos=document.getElementById("datos")
datos.innerHTML=nombre+" "+altura;

// con las comillas invertidas se puede concatenar un html como cuerpo dinamico trabajado desde js
datos.innerHTML=`<h1>INNER</h1>`


/*// hay varias maneras para declarar una funcion en JS
// aqui se usara el modo de ARRW FUNCTION
// son funciones anonimas osea que no se necesita agregar nombre a la funcion
// el objeto argumentos no se encuentra en el contexto de las funcion
var saludar=new Promise((resolve,reject)=>{
    setTimeout(()=>{
        // let saludo="Hola muy buenas a todos chavales!!"
        let saludo=false;
        if(saludo){
            resolve(saludo);
        }else{
            reject('No hay saludo disponible')
        }

    },2000);
})*/


// saludar.then(resultado=>{
//     alert(resultado)
// }).catch(err=>{
//     alert(err)
// })