'use strict'

//CARGAR MODULOS DE NODE PARA CREAR EL SERVIDOR
var express = require('express');
var bodyParser = require('body-parser');

//EJECUTAR EXPRESS PARA PODER TRABAJAR CON HTTP
var app = express();

//CARGAR FICHEROS DE RUTAS
var article_routes=require('./rutas/article');

//CARGAR MIDDLEWARES(SE EJECUTA ANTES DE CARGAR UNA URL)
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json())

//CORS PARA PETICION DESDE FRONTEND
// Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});


//AÑADIR PREFIJOS A LAS RUTAS TIPO HTACCES
app.use('/api',article_routes);
    //RUTA O METODO DE PRUEBA PARA EL API
   /* app.get('/datos-curso',function(request,response){
        // console.log("Hola mundo");
        return response.status(404).send({
            curso:'Master en Framworks Js',
            autor:'Oscar Diaz',
            web:'over-hub.com'
        })
    })*/

//EXPORTAR MODULOS
module.exports=app;