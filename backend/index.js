'use strict';
var mongoose = require('mongoose');
var app=require('./app');
var port= 3900;
mongoose.set("useFindAndModify", false);
mongoose.set("useUnifiedTopology", true);
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/api_rest_blog", {
    useNewUrlParser: true,
  })
  .then(() => {
    console.log("Conexión a la bd se ha establecido");

    //crear servidor y se pone escuchar peticiones http
    app.listen(port,()=>{
        console.log("servidor corriendo en http://localhost:"+port)
    })
  });
