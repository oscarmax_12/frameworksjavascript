'use strict'

var objvalidator = require('validator');
var objArticle = require('../modelos/articles');
var objFs = require('fs');
var objPath = require('path');


var controller = {
    datosCurso: function (request, response) {
        // console.log("Hola mundo");
        return response.status(200).send({
            curso: 'Master en Framworks Js',
            autor: 'Oscar Diaz',
            web: 'over-hub.com'
        });
    },
    test: function (request, response) {
        return response.status(200).send({
            message: 'Soy la accion test de mi controlador de articulos'
        });
    },
    guardarArticulo: function (request, response) {
        // recoger parametros de POST
        var params = request.body;

        //validar datos con validator
        try {
            var tituloValido = !objvalidator.isEmpty(params.title);
            var contentValido = !objvalidator.isEmpty(params.content);

        } catch (error) {
            return response.status(500).send({
                status: 'error',
                message: 'Faltan datos por enviar!!'
            });
        }
        if (tituloValido && contentValido) {
            //crear objeto a guardar
            var article = new objArticle();

            //asignar valores
            article.title = params.title;
            article.content = params.content;
            if (params.image){
                article.image = params.image;
            }else{
                article.image = null;
            }

            //guardar articulo
            article.save(function (error, articleStored) {
                if (error || !articleStored) {
                    return response.status(500).send({
                        status: 'error',
                        message: 'El articulo no se ah guardado'
                    });
                }
                //deolver respuesta 200
                return response.status(200).send({
                    status: 'success',
                    article: articleStored
                });

            })


        } else {
            return response.status(500).send({
                status: 'error',
                message: 'Datos no validos'
            });
        }

    },
    listarArticles: function (request, response) {
        var last = request.params.last;
        var query = objArticle.find({
            //aqui se agregan los filtros cuando se hace una busquedad de un determinado articulo
        });
        if (last || last != undefined) {
            query.limit(5);//filtra solo los 5 ultimos

        }

        //Find sacar los datos de la BD
        query.sort('-_id').exec(function (error, resArticulos) {
            //PARA ORDENAR SE APLICA LO DE SORT CON UN "-" ADELANTE PARA ORDENAR DESCENDENTE
            if (error) {
                return response.status(500).send({
                    status: 'error',
                    message: 'error al devolver datos'
                });
            }
            if (!resArticulos) {
                return response.status(404).send({
                    status: 'error',
                    message: 'No hay Articulos'
                });
            }
            return response.status(200).send({
                status: 'sucess',
                resArticulos
            });
        })

    },
    verArticle: function (request, response) {

        //recoger ell id de la url
        var articleId = request.params.id;

        //comprobar que existe
        if (!articleId || articleId == null) {
            return response.status(404).send({
                status: 'error',
                message: 'Parametro no encontrado en la peticion'
            });
        }

        //Buscar el articulo
        objArticle.findById(articleId, function (error, resArticle) {

            if (!resArticle || error) {
                return response.status(404).send({
                    status: 'error',
                    message: 'No hay nada que devolver'
                });
            }
            //devolver el json
            return response.status(200).send({
                status: 'success',
                resArticle
            });
        });


    },
    editarArticle: function (request, response) {
        //recoger el id
        var articleId = request.params.id;

        var params = request.body;

        try {
            var tituloValido = !objvalidator.isEmpty(params.title);
            var contentValido = !objvalidator.isEmpty(params.content);

        } catch (error) {
            return response.status(500).send({
                status: 'error',
                message: error
            });
        }

        if (tituloValido && contentValido) {
            objArticle.findOneAndUpdate({_id: articleId}, params, {new: true}, function (error, resArticleUpdated) {
                if (error) {
                    return response.status(500).send({
                        status: 'error',
                        message: error + ' ,Error al actualizar'
                    });
                }
                if (!resArticleUpdated) {
                    return response.status(404).send({
                        status: 'error',
                        message: error + ' ,No existe el Articulo'
                    });
                }
                // console.log(resArticleUpdated)
                return response.status(200).send({
                    status: 'success',
                    article: resArticleUpdated
                });
            });

        } else {
            return response.status(500).send({
                status: 'error',
                message: 'validacion no es correcta'
            });
        }

    },
    eliminarArticle: function (request, response) {
        var articleId = request.params.id;

        objArticle.findOneAndDelete({_id: articleId}, function (error, resArticleRemoved) {
            if (error) {
                return response.status(500).send({
                    status: 'error',
                    message: error + ' ,Error al borrar'
                });
            }
            if (!resArticleRemoved) {
                return response.status(404).send({
                    status: 'error',
                    message: error + ' ,No se ha borrado el articulo, posiblemente no exista'
                });
            }
            return response.status(200).send({
                status: 'success',
                article: resArticleRemoved
            });

        })

    },
    upfile: function (request, response) {
        //configurar el modulo connect pmultiparty  router/article.js

        //recoger el fichero de la peticion
        var file_name = 'Imagen no subida..';

        if (!request.files) {
            return response.status(404).send({
                status: 'error',
                message: file_name
            });
        }
        //conseguir el nombre y la extencion del archivo

        var file_path = request.files.file0.path;//se usa este nombre por el uso de librerias en el front que envian ese nombre

        var file_split = file_path.split('\\');

        //***ADVENTERCIA EN LINUX O MAC***
        //var file_split=file_path.split('/'); // se cambia la diagonal invertida por diagonal normal

        //Nombre del archivo
        var file_name = file_split[2];

        //Extension de archivo
        var extension_split = file_name.split('\.');
        var file_extension = extension_split[1];

        //comprobar la extension, solo imagenes, si no es valido se borra el fichero
        if (file_extension != 'png' && file_extension != 'jpg' && file_extension != 'jpeg' && file_extension != 'gif') {
            //borrar el archivo de paso
            objFs.unlink(file_path, function (error) {
                return response.status(500).send({
                    status: 'error',
                    message: error + 'Error al borrar el archivo'
                });
            })

        } else {
            //SI TODO ES VALIDO

            var articleId = request.params.id;

            if (articleId) {
                //buscar el articulo, asignarle el nombre de la imagen y actualizarlo
                console.log("parametros de paso: " + articleId + " " + file_name + " ")
                objArticle.findOneAndUpdate({_id: articleId}, {image: file_name}, {new: true}, function (err, resArticleUpdated) {
                    console.log("entro al findonby")
                    if (err || !resArticleUpdated) {
                        return response.status(500).send({
                            status: 'error',
                            message: 'Error al subir el archivo'
                        });
                    }
                    console.log("entro al findonby VALIDAR")
                    return response.status(200).send({
                        status: 'success',
                        article: resArticleUpdated
                    });
                });
            }else{
                return response.status(200).send({
                    status: 'success',
                    image: file_name
                });
            }


            // return response.status(500).send({
            //     fichero:request.files,
            //     split:file_split,
            //     file_extension
            // });
        }//end upload file


        //solo images
        // var articleId=request.params.id;
    },
    getImage: function (request, response) {
        //nombre del fichero
        var file_name = request.params.image;
        var path_file = './upload/articles/' + file_name;

        objFs.exists(path_file, function (exists) {
            if (exists) {
                return response.sendFile(objPath.resolve(path_file));
            } else {
                return response.status(404).send({
                    status: 'error',
                    message: 'La imagen no existe en el banco de imagenes'
                });
            }
        })

        // return response.status(200).send({
        //     // fichero:request.files,
        //     // split:file_split,
        //     // file_extension
        // });
    },
    search: function (request, response) {
        var param_search = request.params.p_search;
        objArticle.find({
            "$or": [
                {"title": {"$regex": param_search, "$options": "i"}},
                {"content": {"$regex": param_search, "$options": "i"}}//i significa si esta incluido, buscalo o muestralo
            ]

        })
            .sort([['date', 'descending']])
            .exec(function (error, resarticles) {
                if (error) {
                    return response.status(500).send({
                        status: 'error',
                        message: 'Error en la peticion: ' + error
                    });
                }
                if (!resarticles || resarticles.length <= 0) {
                    return response.status(404).send({
                        status: 'error',
                        message: 'No se ah encontrado nada con " ' + param_search + ' "'
                    });
                }
                return response.status(200).send({
                    status: 'success',
                    articles: resarticles
                });
            })


    }


};
module.exports = controller;