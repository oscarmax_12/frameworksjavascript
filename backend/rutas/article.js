'use strict'

var express=require('express');
var ArticleController=require('../controladores/articleControlador');
var router=express.Router();

var multipart=require('connect-multiparty');
var md_upload=multipart({uploadDir:'./upload/articles'});

// RUTAS DE PRUEBA
router.get('/test-de-controlador',ArticleController.test);
router.post('/datos-curso',ArticleController.datosCurso);

// RUTAS UTILES
router.post('/guardarArticulo',ArticleController.guardarArticulo);
router.get('/listarArticles/:last?',ArticleController.listarArticles);
router.get('/verArticle/:id',ArticleController.verArticle);
router.put('/editarArticle/:id',ArticleController.editarArticle);
router.delete('/eliminarArticle/:id',ArticleController.eliminarArticle);

router.post('/upfile-image/:id?',md_upload,ArticleController.upfile);
router.get('/getimage/:image',ArticleController.getImage);
router.get('/search/:p_search',ArticleController.search);

// console.log('md_upload '+ArticleController.upfile);


module.exports=router;