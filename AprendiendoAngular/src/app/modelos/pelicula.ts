export class Pelicula {
  // public title:String;
  // public year:number;
  // public image:String;

  // constructor(title,year,image){
  //   this.title=title;
  //   this.year=year;
  //   this.image=image;
  // }

  /// DECLARAR TODOS LAS VARIABLES EN EL CONSTRUCTOR HACE LO MISMO QUE LO DE ARRIBA
  constructor(
    public title: String,
    public year: number,
    public image: String) {

  }
}
