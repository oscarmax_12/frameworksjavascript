import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ArticleService} from 'src/app/services/article.service';
import {Article} from 'src/app/modelos/article';
import {Global} from 'src/app/services/global';
import swal from 'sweetalert2';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css'],
  providers: [ArticleService]
})
export class ArticleDetailComponent implements OnInit {

  public article: Article;

  public url: string;


  constructor(private _articleService: ArticleService, private _route: ActivatedRoute, private _router: Router) {
    this.url = Global.URL_WS;
  }

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      let id = params['id'];
      this._articleService.detalleArticulo(id).subscribe(
        response => {

          if (response.resArticle) {
            this.article = response.resArticle;

          } else {
            this._router.navigate(['/home']);
          }
        },
        error => {
          console.log(error);
        }
      );
    });

  }

  eliminarArticulo(idArticulo: string): void {

    swal.fire({
      title: 'Estas seguro?',
      text: 'Estas seguro de Eliminar este articulo?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, borralo',
      cancelButtonText: 'No lo elimines'
    }).then((result) => {
      if (result.isConfirmed) {


        this._articleService.borrarArticulo(idArticulo).subscribe(
          response => {
            swal.fire({
              position: 'top-end',
              icon: 'success',
              title: 'Articulo Eliminado',
              text: 'El articulo se ah eliminado'
            });
            this._router.navigate(['/blog']);

          },
          error => {
            swal.fire({icon: 'error', title: 'Oops...', text: error});
          }
        );
      }
    });


  }
}
