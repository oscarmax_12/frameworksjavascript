import { Component, OnInit } from '@angular/core';
import {Article} from 'src/app/modelos/article';
import { ArticleService } from 'src/app/services/article.service';
import { Global } from 'src/app/services/global';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[ArticleService]
})
export class HomeComponent implements OnInit {
  public articles:Article[];
  public url:string

  constructor(private _articleService:ArticleService) {
    this.url=Global.URL_WS;
   }

  ngOnInit(): void {
    this._articleService.listarArticles(true).subscribe(
      response=>{
        if (response.resArticulos) {
          this.articles=response.resArticulos;
          console.log(this.articles)
        } else {

        }
      },
      error=>{
        console.log(error)
      }
    )
  }

}
