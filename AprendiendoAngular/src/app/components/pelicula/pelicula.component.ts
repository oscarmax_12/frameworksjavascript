import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { Pelicula } from 'src/app/modelos/pelicula';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  @Input() pelicula:Pelicula;//para el evento hijo
  @Output() MarcarFavorita=new EventEmitter();//para el evento padre

  constructor() { }

  ngOnInit(): void {
  }

  seleccionar(event,pelicula){
    // console.log(event)
    // console.log(pelicula)
    this.MarcarFavorita.emit({
      pelicula:pelicula
    });
  }

}
