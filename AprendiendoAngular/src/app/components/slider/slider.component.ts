import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  @Input() public nombre:String;
  @Input() public size:String;

  constructor() { }

  ngOnInit(): void {
  }

}
