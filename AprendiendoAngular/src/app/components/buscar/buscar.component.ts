import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Article} from '../../modelos/article';
import {ArticleService} from '../../services/article.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css'],
  providers: [ArticleService]
})
export class BuscarComponent implements OnInit {

  public objArticulos: Article[];
  public mBuscarArticulo: string;

  constructor(private _route: ActivatedRoute, private _router: Router, private _articleService: ArticleService) {
  }

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.mBuscarArticulo = params['search'];
      // alert(buscar);
      this._articleService.buscar(this.mBuscarArticulo).subscribe(
        response => {
          // console.log(response);
          if (response.articles) {
            this.objArticulos = response.articles;
            // console.log(this.objArticulos);
          } else {
            this.objArticulos = [];
          }
        },
        error => {
          console.log('error', error);
          alert(error.error.message);// depende de la respuesta del backend
          this.mBuscarArticulo = error.error.message;
          this.objArticulos = [];
          /*this._router.navigate(['/home']);*/
        }
      );
    });
  }

}
