import { Component } from "@angular/core";

@Component({
  selector: 'mi-componente',
  templateUrl:'./mi-componente.component.html'
})
export class MiComponente{
public titulo:string;
public comentario:string;
public year:number;


  constructor(){
    this.titulo="Hola este el constructor de MI COMPONENTE";
    this.comentario="este es el comentario desde el constructor";
    this.year=2020;
    console.log("Mi componente cargado");
    console.log(this.titulo);
    console.log(this.comentario);
    console.log(this.year);
  }
}
