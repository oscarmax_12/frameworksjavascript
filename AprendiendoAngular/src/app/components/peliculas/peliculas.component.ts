import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';
import { Pelicula } from '../../modelos/pelicula';
import {PeliculaService} from '../../services/pelicula.service';

@Component({
  selector: 'peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css'],
  providers:[PeliculaService]
})
export class PeliculasComponent implements OnInit, DoCheck, OnDestroy {

  //OnInit,Docheck...son Hooks y funcionan como eventos para cada componente

  public titulo: string;
  public peliculas: Pelicula[];
  public favorita: Pelicula;
  public fecha: any;

  constructor(
    private _peliculaService:PeliculaService
  ) {
    this.titulo = "componente titulo peliculas"
    this.peliculas = this._peliculaService.getPeliculas();
    this.fecha=new Date(2020,8,12);
    //  console.log("contructor iniciado")
  }

  ngOnInit(): void {
    //como buena practica es una logica aqui y no en el constructor
    //console.log("Coponente OnIniti iniciado")
    console.log(this._peliculaService.holaMundo)
  }

  ngDoCheck() {
    //console.log("Docheck Lanzado")
  }
  ngOnDestroy() {
    //console.log("El componente se va a eliminar")
  }

  cambiarTitulo() {
    //this.titulo="El titulo ah sido cambiado";
  }
  mostrarFavorita(event) {

    this.favorita = event.pelicula;
    console.log(this.favorita)
    // console.log("sin this "+favorita)
    console.log(this.favorita.title)
  }
}
