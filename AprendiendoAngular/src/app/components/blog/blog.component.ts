import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../../services/article.service'
import { Article } from '../../modelos/article';
import { Global } from '../../services/global';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers:[ArticleService]
})
export class BlogComponent implements OnInit {

  public articles:Article[];
  public url:string


  constructor(
    private _articleService:ArticleService
  ) {
    this.url=Global.URL_WS;
   }

  ngOnInit(): void {
    this._articleService.listarArticles().subscribe(
      response=>{
        if (response.resArticulos) {
          this.articles=response.resArticulos;
        } else {

        }
      },
      error=>{
        console.log(error)
      }
    )
  }

}
