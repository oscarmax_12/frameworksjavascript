import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public mBuscarString: string;

  constructor(
    private objRouter: Router,
    private objRouteActivated: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
  }

  public  buscarArticulo(): void {
    // se hace la redireccion a la magiona de redireccion con el paso de parametro concatenado mBuscarString;
    this.objRouter.navigate(['/buscar', this.mBuscarString]);
  }
}
