import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-pagina',
  templateUrl: './pagina.component.html',
  styleUrls: ['./pagina.component.css']
})
export class PaginaComponent implements OnInit {

  public nombre:String;
  public apellidos:String;

  constructor(private _route: ActivatedRoute, private _router: Router) { }

  ngOnInit(): void {
    //recoger los parametros de paso de la url
    // this._route.params.subscribe(function (params: Params) {
    //  this.nombre=params.nombre;
    //   //  console.log("los parametros son " + params.nombre);
    // });
    this._route.params.subscribe((params:Params)=>{
      this.nombre=params.nombre;
      this.apellidos=params.apellidos;
    })
  }
  redireccion(){
    // this._router.navigate(['/formulario']);
    this._router.navigate(['/pagina-de-pruebas','Oscar Fernando','Diaz anchay']);
    // alert("metodo redireccion")
  }

}
