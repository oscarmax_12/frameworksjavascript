import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Article} from '../../modelos/article';
import {ArticleService} from '../../services/article.service';
import {Global} from '../../services/global';
import swal from 'sweetalert2';

@Component({
  selector: 'app-articulo-editar',
  templateUrl: '../nuevo-articulo/nuevo-articulo.component.html',
  styleUrls: ['./articulo-editar.component.css'],
  providers: [ArticleService]
})
export class ArticuloEditarComponent implements OnInit {

  public objArticulo: Article;
  public estado: string;
  private articuloService: ArticleService;
  private route: ActivatedRoute;
  private router: Router;
  public isEdit: boolean;
  public tituloPag: string;
  public url: string;

  afuConfig = {
    multiple: false,
    formatsAllowed: '.jpg,.png,.gif,.jpeg',
    maxSize: '5',
    uploadAPI: {
      url: Global.URL_WS + 'upfile-image'
    },
    theme: 'attachPin',
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Selecionar Imagen',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !',
      sizeLimit: 'Size Limit'
    }
  };

  constructor(route: ActivatedRoute, router: Router, articuloService: ArticleService) {
    this.objArticulo = new Article('', '', '', null, null);
    this.articuloService = articuloService;
    this.route = route;
    this.router = router;
    this.isEdit = true;
    this.tituloPag = 'Editar Articulo';
    this.url = Global.URL_WS;
  }

  ngOnInit(): void {
    this.getArticulo();
    // swal.fire('Registro exitoso...', 'Articulo editado', 'success');
  }

  public onSubmitEnviarData(): void {


    swal.fire({
      title: 'Estas Seguro?',
      text: 'Seguro de editar este articulo?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, editalo!',
      cancelButtonText: 'No lo edites'
    }).then((result) => {
      if (result.isConfirmed) {
        this.articuloService.actualizarArticulo(this.objArticulo._id, this.objArticulo).subscribe(
          response => {
            if (response.status === 'success') {
              this.estado = 'success';
              this.objArticulo = response.article;
              console.log(response);
              swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Articulo Editado',
                text: 'El articulo se ah editado'
              });

              this.router.navigate(['/blog/articulo', this.objArticulo._id]);
            } else {
              this.estado = 'error';
            }

          },
          error => {
            this.estado = 'error';
            swal.fire({icon: 'error', title: 'Oops...', text: error});
          }
        );
      }
    });
  }

  imagenCargada($event: any): void {
    console.log($event);
    console.log($event.body);
    console.log($event.body.image);

    this.objArticulo.image = $event.body.image;

  }

  getArticulo(): void {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.articuloService.detalleArticulo(id).subscribe(
        response => {

          if (response.resArticle) {
            this.objArticulo = response.resArticle;

          } else {
            this.router.navigate(['/home']);
          }
        },
        error => {
          console.log(error);
        }
      );
    });
  }
}
