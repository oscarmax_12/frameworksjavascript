import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Article} from '../../modelos/article';
import {ArticleService} from '../../services/article.service';
import {Global} from '../../services/global';
import swal from 'sweetalert2';


@Component({
  selector: 'app-nuevo-articulo',
  templateUrl: './nuevo-articulo.component.html',
  styleUrls: ['./nuevo-articulo.component.css'],
  providers: [ArticleService]
})
export class NuevoArticuloComponent implements OnInit {

  public objArticulo: Article;
  public estado: string;
  private articuloService: ArticleService;
  private route: ActivatedRoute;
  private router: Router;
  public tituloPag: string;
  public isEdit: boolean;

  afuConfig = {
    multiple: false,
    formatsAllowed: '.jpg,.png,.gif,.jpeg',
    maxSize: '5',
    uploadAPI: {
      url: Global.URL_WS + 'upfile-image'
    },
    theme: 'attachPin',
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: false,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Selecionar Imagen',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !',
      sizeLimit: 'Size Limit'
    }
  };

  constructor(route: ActivatedRoute, router: Router, articuloService: ArticleService) {
    this.objArticulo = new Article('', '', '', null, null);
    this.articuloService = articuloService;
    this.route = route;
    this.router = router;
    this.tituloPag = 'Nuevo Articulo';
    this.isEdit = false;
  }

  ngOnInit(): void {
  }

  public onSubmitEnviarData(): void {
    this.articuloService.guardarArticulo(this.objArticulo).subscribe(
      response => {
        if (response.status === 'success') {
          this.estado = 'success';
          this.objArticulo = response.article;
          console.log(response);
          swal.fire('Registro exitoso...', 'Articulo Agregado', 'success');

          this.router.navigate(['/blog']);
        } else {
          this.estado = 'error';
        }

      },
      error => {
        this.estado = 'error';
      }
    )
    ;
  }

  // imagenCargada(data): void {
  //   console.log(data);
  //   // console.log(data.response);
  //   console.log(data.body.imagen);
  //   // console.log(data.imagen);
  //   const imgData = JSON.parse(data.body);
  //   console.log(imgData);
  // }
  imagenCargada($event: any): void {
    console.log($event);
    console.log($event.body);
    console.log($event.body.image);
    // console.log(data.imagen);
    // const imgData = JSON.parse($event.body);
    this.objArticulo.image = $event.body.image;
    // console.log(imgData.image);
    // alert($event.body.image);
  }
}
