import { Component } from '@angular/core';

@Component({
  selector: 'app-root', // es la etiqueta para llamarla desde la vista
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'Aprendiendo Angular con Oscar diaz';

}
