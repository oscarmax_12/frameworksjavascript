// importar los modulos del router de angular

import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Importar los componentes a los cuales les quiero hacer una pagina exclusiva

import {HomeComponent} from './components/home/home.component';
import {BlogComponent} from './components/blog/blog.component';
import {FormularioComponent} from './components/formulario/formulario.component';
import {PeliculasComponent} from './components/peliculas/peliculas.component';
import {PaginaComponent} from './components/pagina/pagina.component';
import {ArticleDetailComponent} from './components/article-detail/article-detail.component';
import {ErrorComponent} from './components/error/error.component';
import {BuscarComponent} from './components/buscar/buscar.component';
import {NuevoArticuloComponent} from './components/nuevo-articulo/nuevo-articulo.component';
import {ArticuloEditarComponent} from './components/articulo-editar/articulo-editar.component';

// Array de rutas

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'blog/articulo/:id', component: ArticleDetailComponent},
  {path: 'blog/crear', component: NuevoArticuloComponent},
  {path: 'blog/editar/:id', component: ArticuloEditarComponent},
  {path: 'buscar/:search', component: BuscarComponent},
  {path: 'formulario', component: FormularioComponent},
  {path: 'peliculas', component: PeliculasComponent},
  {path: 'pagina-de-pruebas', component: PaginaComponent},
  {path: 'pagina-de-pruebas/:nombre/:apellidos', component: PaginaComponent},
  {path: '**', component: ErrorComponent}
];

// exportar el modulo de rutas

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);
