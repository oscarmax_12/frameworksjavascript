import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
// modificacion de module app
import {routing, appRoutingProviders} from './app.routing';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MomentModule} from 'angular2-moment';
import {AngularFileUploaderModule} from 'angular-file-uploader';
// modificacion de module app

import {AppComponent} from './app.component';
import {MiComponente} from './components/mi-componente/mi-componente.component';
import {PeliculasComponent} from './components/peliculas/peliculas.component';
import {PruebasComponent} from './components/pruebas/pruebas.component';
import {HeaderComponent} from './components/header/header.component';
import {SliderComponent} from './components/slider/slider.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {FooterComponent} from './components/footer/footer.component';
import {HomeComponent} from './components/home/home.component';
import {BlogComponent} from './components/blog/blog.component';
import {FormularioComponent} from './components/formulario/formulario.component';
import {PaginaComponent} from './components/pagina/pagina.component';
import {ErrorComponent} from './components/error/error.component';
import {PeliculaComponent} from './components/pelicula/pelicula.component';
import {EsParPipe} from './pipes/espar.pipe';
import {ArticlesComponent} from './components/articles/articles.component';
import {ArticleDetailComponent} from './components/article-detail/article-detail.component';
import {BuscarComponent} from './components/buscar/buscar.component';
import {NuevoArticuloComponent} from './components/nuevo-articulo/nuevo-articulo.component';
import { ArticuloEditarComponent } from './components/articulo-editar/articulo-editar.component';

@NgModule({
  declarations: [
    AppComponent,
    MiComponente,
    PeliculasComponent,
    PruebasComponent,
    HeaderComponent,
    SliderComponent,
    SidebarComponent,
    FooterComponent,
    HomeComponent,
    BlogComponent,
    FormularioComponent,
    PaginaComponent,
    ErrorComponent,
    PeliculaComponent,
    EsParPipe,
    ArticlesComponent,
    ArticleDetailComponent,
    BuscarComponent,
    NuevoArticuloComponent,
    ArticuloEditarComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpClientModule,
    MomentModule,
    AngularFileUploaderModule
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule {
}
