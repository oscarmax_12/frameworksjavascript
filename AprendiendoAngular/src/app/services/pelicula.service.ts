import {Injectable} from '@angular/core';
import {Pelicula} from '../modelos/pelicula';

@Injectable()
export class PeliculaService{
  public peliculas:Pelicula[];

  constructor(){
    this.peliculas=[
      new Pelicula("Spiderman 4", 2014, "https://hipertextual.com/files/2019/06/hipertextual-4-trajes-spider-man-lejos-casa-nuevo-trailer-2019196176.jpg"),
      new Pelicula("Los vengadores End Game", 2017, "https://i.blogs.es/876f04/los-vengadores-4/1366_2000.jpg"),
      new Pelicula("Batman vs Superman", 2015, "https://img2.rtve.es/v/3535888?w=1600&preview=1458729594522.jpg"),
      new Pelicula("Batman vs Superman", 2014, "https://img2.rtve.es/v/3535888?w=1600&preview=1458729594522.jpg"),
      new Pelicula("Batman vs Superman", 2012, "https://img2.rtve.es/v/3535888?w=1600&preview=1458729594522.jpg")
    ];
  }

  holaMundo(){
    return 'Hola mundo desde el servicio en angular';
  }
  getPeliculas(){
return this.peliculas;
  }
}
