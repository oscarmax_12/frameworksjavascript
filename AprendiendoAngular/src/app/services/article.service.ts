import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Article} from '../modelos/article';
import {Observable} from 'rxjs';
import {Global} from './global';

@Injectable()
export class ArticleService {

  public url: string;

  constructor(
    private _http: HttpClient
  ) {
    this.url = Global.URL_WS;
  }

  /*  pruebas() {
      return 'Soy el servicio de Articulos';
    }*/

  listarArticles(last: any = null): Observable<any> {
    var articles: string = 'listarArticles';
    if (last != null) {
      // valido si es que me mandan las varibale last para hacer una peticion distinta al enrutador controller
      articles = 'listarArticles/true';
    }

    return this._http.get(this.url + articles); // aqui hace la peticion AJAX  al backend
  }

  detalleArticulo(articleId): Observable<any> {
    return this._http.get(this.url + 'verArticle/' + articleId);
  }

  buscar(stringBuscar): Observable<any> {
    return this._http.get(this.url + 'search/' + stringBuscar);
  }

  guardarArticulo(objArticulo): Observable<any> {
    const params = JSON.stringify(objArticulo);
    const cabeceras = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.post(this.url + 'guardarArticulo', params, {headers: cabeceras});
  }

  actualizarArticulo(id, objArticulo: Article): Observable<any> {
    const params = JSON.stringify(objArticulo);
    const cabeceras = new HttpHeaders().set('Content-type', 'application/json');
    return this._http.put(this.url + 'editarArticle/' + id, params, {headers: cabeceras});
  }

  borrarArticulo(id): Observable<any> {
    const cabeceras = new HttpHeaders().set('Content-type', 'application/json');
    return this._http.delete(this.url + 'eliminarArticle/' + id, {headers: cabeceras});
  }

}
